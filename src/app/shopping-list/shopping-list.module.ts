import { NgModule } from "@angular/core";
import { ShoppingEditComponent } from "./shopping-edit/shopping-edit.component";
import { ShoppingListRoutingModule } from "./shopping-list-routing.module";
import { ShoppingListComponent } from "./shopping-list.component";

@NgModule({
  declarations: [ShoppingEditComponent, ShoppingListComponent],
  imports: [ShoppingListRoutingModule]
})
export class ShoppingListModule {}
