import { NgModule } from "@angular/core";
import { RecipeListComponent } from "./recipe-list/recipe-list.component";
import { RecipeItemComponent } from "./recipe-list/recipe-item/recipe-item.component";
import { RecipesComponent } from "./recipes.component";
import { RecipesRoutingModule } from "./recipes-routing.module";

@NgModule({
  declarations: [RecipeListComponent, RecipesComponent, RecipeItemComponent],
  imports: [RecipesRoutingModule]
})
export class RecipesModule {}
