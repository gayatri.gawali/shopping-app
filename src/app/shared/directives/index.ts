import { BasicHighLighterDirective } from "./basicHighlighterDirective";
import { UnlessStructuralDirective } from "./unless-structural.directive";

export const directives: any[] = [
  BasicHighLighterDirective,
  UnlessStructuralDirective
];

export { BasicHighLighterDirective } from "./basicHighlighterDirective";
export { UnlessStructuralDirective } from "./unless-structural.directive";
