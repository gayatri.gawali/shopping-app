// this is custom structural directive works opposite of ngIf directive i.e when true it hides element

import { Directive, Input, TemplateRef, ViewContainerRef } from "@angular/core";

@Directive({
  selector: "[appUnless]"
})
export class UnlessStructuralDirective {
  @Input() set appUnless(condition: string) {
    if (!condition) {
      this.vcRef.createEmbeddedView(this.temRef);
    } else {
      this.vcRef.clear();
    }
  }
  constructor(
    private temRef: TemplateRef<any>,
    private vcRef: ViewContainerRef
  ) {}
}
