import {
  Directive,
  ElementRef,
  OnInit,
  Renderer2,
  HostListener,
  HostBinding,
  Input
} from "@angular/core";

@Directive({
  selector: "[appHighlighter]"
})
export class BasicHighLighterDirective implements OnInit {
  @Input() defaultColor = "transparent";
  @Input("appHighlighter") highlightColor = "pink";
  @HostBinding("style.backgroundColor") backgroundColor: string;
  constructor(private renderer: Renderer2, private eleRef: ElementRef) {}
  ngOnInit(): void {
    this.backgroundColor = this.defaultColor;
    // this.renderer.setStyle(
    //   this.eleRef.nativeElement,
    //   "background-color",
    //   "green"
    // );
  }
  @HostListener("mouseenter") mouseEnter(eventData: Event) {
    this.backgroundColor = this.highlightColor;
    // this.renderer.setStyle(
    //   this.eleRef.nativeElement,
    //   "background-color",
    //   "green"
    // );
  }
  @HostListener("mouseleave") mouseLeave(eventData: Event) {
    this.backgroundColor = this.defaultColor;
    //   this.eleRef.nativeElement,
    //   "background-color",
    //   "transparent"
    // );
  }
}
