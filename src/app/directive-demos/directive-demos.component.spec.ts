import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectiveDemosComponent } from './directive-demos.component';

describe('DirectiveDemosComponent', () => {
  let component: DirectiveDemosComponent;
  let fixture: ComponentFixture<DirectiveDemosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectiveDemosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectiveDemosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
