import { Component, OnInit } from "@angular/core";
import { environment } from "@env";

@Component({
  selector: "app-directive-demos",
  templateUrl: "./directive-demos.component.html",
  styleUrls: ["./directive-demos.component.scss"]
})
export class DirectiveDemosComponent implements OnInit {
  demo = environment.DEMO;
  oddNumbers = [1, 3, 5];
  evenNumbers = [2, 4, 6];
  onlyOdd = true;
  constructor() {}
  ngOnInit(): void {}
  toggleOddEven() {
    this.onlyOdd = !this.onlyOdd;
  }
}
