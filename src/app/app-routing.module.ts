import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { DirectiveDemosComponent } from "./directive-demos/directive-demos.component";
export const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },

  {
    path: "demo",
    component: DirectiveDemosComponent
  },
  {
    path: "recipes",
    loadChildren: () =>
      import("./recipes/recipes.module").then(module => module.RecipesModule)
  },
  {
    path: "shopping",
    loadChildren: () =>
      import("./shopping-list/shopping-list.module").then(
        module => module.ShoppingListModule
      )
  },
  {
    path: "**",
    redirectTo: "shopping",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
