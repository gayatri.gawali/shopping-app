import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { LoginService, AuthResponse } from "./login.service";
import { Subscription, Observable } from "rxjs";
import { tap, map, finalize } from "rxjs/operators";
import { UserModel } from "./user.model";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit, OnInit {
  isLoginMode = true;
  subscriptions: Subscription[] = [];
  errorMessage: any;
  constructor(private loginService: LoginService, private router: Router) {}

  ngOnInit() {}
  onSwitchMode() {
    this.isLoginMode = !this.isLoginMode;
  }
  onSubmit(loginForm: NgForm) {
    this.errorMessage = "";
    console.log("data", loginForm.value);
    let observer: Observable<AuthResponse>;
    let userObject: UserModel;
    if (!loginForm.valid) {
      return;
    }
    observer = this.isLoginMode
      ? this.signIn(loginForm)
      : this.signUp(loginForm);
    this.subscriptions.push(
      observer.subscribe(
        result => {
          console.log("user data:", result);
          this.router.navigate(["recipes"]);
        },
        errorMessage => {
          console.log("error:", errorMessage);
          this.errorMessage = errorMessage;
        }
      )
    );
    loginForm.reset();
  }
  signIn(loginForm: NgForm): Observable<AuthResponse> {
    // this.subscriptions.push(
    return this.loginService.signIn(
      loginForm.controls["email"].value,
      loginForm.controls["password"].value
    );
    // .subscribe(
    //   result => {
    //     console.log("user data:", result);
    //   },
    //   error => console.log("error:", error)
    // )
    // );
  }
  private signUp(loginForm: NgForm): Observable<AuthResponse> {
    // this.subscriptions.push(
    return this.loginService.signUp(
      loginForm.controls["email"].value,
      loginForm.controls["password"].value
    );
    // .subscribe(
    //   result => {
    //     console.log("user data:", result);
    //     this.isLoginMode = !this.isLoginMode;
    //   },
    //   error => console.log("error:", error)
    // )
    // );
  }
  onDestroy() {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }
}
