import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError, BehaviorSubject } from "rxjs";
import { catchError, tap, finalize } from "rxjs/operators";
import { UserModel } from "./user.model";
export interface AuthResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}
@Injectable({
  providedIn: "root"
})
export class LoginService {
  user = new BehaviorSubject<UserModel>(null);
  constructor(private http: HttpClient) {}
  signUp(email: string, password: string): Observable<AuthResponse> {
    return this.http
      .post<AuthResponse>(
        "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDPj7b78A3StQGRpeppXGSFBDXkWah5T1I",
        {
          email,
          password,
          returnSecureToken: true
        }
      )
      .pipe(
        catchError(errorRes => {
          return this.handleError(errorRes);
        }),
        tap(resData => {
          this.handleAuthentication(resData);
        })
      );
  }

  private handleAuthentication(resData: AuthResponse) {
    const expirationDate = new Date().getTime() + +resData.expiresIn * 3600;
    const userObject = new UserModel(
      resData.email,
      resData.localId,
      resData.idToken,
      new Date(expirationDate)
    );
    this.user.next(userObject);
  }

  signIn(email: string, password: string): Observable<AuthResponse> {
    return this.http
      .post<AuthResponse>(
        "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDPj7b78A3StQGRpeppXGSFBDXkWah5T1I",
        {
          email,
          password,
          returnSecureToken: true
        }
      )
      .pipe(
        catchError(this.handleError),
        tap(
          resData => {
            this.handleAuthentication(resData);
          },
          finalize(() => console.log("completed"))
        )
      );
  }
  handleError(errorRes: HttpErrorResponse) {
    let errorMsg = "unknown error";
    if (!errorRes.error && !errorRes.error.error) {
      return throwError(errorMsg);
    }

    switch (errorRes.error.error.message) {
      case "EMAIL_EXISTS":
        errorMsg = "email already exist";
        break;
      case "OPERATION_NOT_ALLOWED":
        errorMsg = "operation not allowed";
        break;
      case "TOO_MANY_ATTEMPTS_TRY_LATER":
        errorMsg = "to many attempts..try again later";
        break;
      case "EMAIL_NOT_FOUND":
        errorMsg = "email is not found";
        break;
      case "INVALID_PASSWORD":
        errorMsg = "invalid password";
        break;
      case "USER_DISABLED":
        errorMsg = "account is disabled by admin";
        break;
    }
    return throwError(errorMsg);
  }
}
