export class UserModel {
  // idToken: string;
  // email: string;
  // refreshToken: string;
  // expiresIn: string;
  // localId: string;
  // expirationDate: Date;
  // registered: boolean;
  constructor(
    public email: string,
    public localId: string,
    private idToken: string,
    private tokenExpirationDate: Date
  ) // registered: boolean
  {
    // this.registered = registered;
  }
  get token() {
    if (new Date() > this.tokenExpirationDate) {
      return null;
    } else {
      return this.idToken;
    }
  }
}
