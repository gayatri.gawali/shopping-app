import { Component, OnInit } from "@angular/core";
import { Subscription } from 'rxjs';
import { LoginService } from 'app/login/login.service';

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  isAuthenticated=false;
  constructor(private loginService:LoginService) {}
  subscriptions:Subscription[]=[];
  ngOnInit() {
    this.subscriptions.push(
      this.loginService.user.subscribe(user => {
        this.isAuthenticated = !!user
      })
    );
  }
}
